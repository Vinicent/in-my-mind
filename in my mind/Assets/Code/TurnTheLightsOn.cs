using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnTheLightsOn : MonoBehaviour
{
    public GameObject green;
    public GameObject pink;
    public GameObject blue;
    public GameObject yellow;
    
    void Start()
    {
        StartCoroutine(LightCoroutine());
    }

    IEnumerator LightCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        green.SetActive(true);
        
        yield return new WaitForSeconds(0.5f);
        pink.SetActive(true);
        
        yield return new WaitForSeconds(0.5f);
        blue.SetActive(true);
        
        yield return new WaitForSeconds(0.5f);
        yellow.SetActive(true);
        

    }
}
